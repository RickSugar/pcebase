

Key.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Key.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw keyVisible, keyCollected, keyDisappear


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            keyVisible:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 1
                    ;frame table
                    .dw keyVisible.1
                    
                    keyVisible.1:
                        ; delay in frames
                        .db 3
                        ; table
                        .dw keyVisible.meta.1, keyVisible.damageCollision.1, keyVisible.attackCollision.1

                        keyVisible.meta.1:
                            .db 1
                            .dw keyVisible.cell.1

                                keyVisible.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL10
                                    addVramOffset 0
                                    rts

                        keyVisible.damageCollision.1:
                        keyVisible.attackCollision.1:
                            .dw keyVisible.a_box.1.1

                                keyVisible.a_box.1.1:
                                    ;X1, Y1, width, Height, Type
                                    .db 3, 3, 12, 8, 0


            ;//.....................
            ;//                    .
            ;//.....................
            keyCollected:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            keyDisappear:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Key.methodTable:
            .dw Key.method.attack, Key.method.damage, Key.method.death, Key.method.spawn
            .dw Key.method.virtualWindowCheck
            .dw Key.method.AI, Key.method.resetAI, Key.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Key.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.AI:
            jsr Key.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Key.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Key.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Key.class
            sta object.self.lo,x 
            lda #>Key.class
            sta object.self.hi,x 
            lda #^Key.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Key.methodTable
            sta object.methods.lo,x
            lda #>Key.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Key.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Key.method.animate:

Key.frame = R0
Key.frame.entryTable = R1
Key.frame.len = D0
Key.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Key.AnimeTable+1,y
            sta <Key.frame
            lda Key.AnimeTable+2,y
            sta <Key.frame+1

            cly
            ;Get frame type
            lda [Key.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Key.frame],y
            iny
            sta Key.frame.len

            stz <Key.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Key.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Key.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Key.frame],y
            iny
            sta <Key.frame.entryTable
            lda [Key.frame],y
            iny
            sta <Key.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Key.frame.entryTable],y
            iny
            sta <D1.h
            lda <Key.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Key.cellTable        = A0
Key.cell.entry       = A1
Key.attackColPointer = A2
Key.damageColPointer = A3

            ; Intra-frame data
            lda [Key.frame.entryTable],y
            iny
            sta <Key.cellTable
            lda [Key.frame.entryTable],y
            iny
            sta <Key.cellTable+1
            lda [Key.frame.entryTable],y
            iny
            sta <Key.damageColPointer
            lda [Key.frame.entryTable],y
            iny
            sta <Key.damageColPointer+1
            lda [Key.frame.entryTable],y
            iny
            sta <Key.attackColPointer
            lda [Key.frame.entryTable],y
            iny
            sta <Key.attackColPointer+1

Key.cellCount = D0.l
Key.HWsprIdx = D0.h
Key.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Key.HWsprIdx
            cly
            lda [Key.cellTable],y
            iny
            sta <Key.cellCount

.decodeCellLoop
            lda [Key.cellTable],y
            iny
            sta <Key.addCell
            lda [Key.cellTable],y
            iny
            sta <Key.addCell+1
                phy
            ldy <Key.HWsprIdx 
            jsr.ind Key.addCell
                ply

            dec <Key.cellCount
          beq .buildDamageBox
            inc <Key.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Key.class.size = * - Key.class