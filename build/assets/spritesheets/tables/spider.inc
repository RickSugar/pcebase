

Spider.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Spider.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw spiderSlide, spiderDamage, spiderDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            spiderSlide:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 2
                    ;frame table
                    .dw spiderSlide.1, spiderSlide.2
                    
                    ;Open wings
                    spiderSlide.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw spiderSlide.meta.1, spiderSlide.damageCollision.1, spiderSlide.attackCollision.1

                        spiderSlide.meta.1:
                            .db 1
                            .dw spiderSlide.cell.1

                                spiderSlide.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL7
                                    addVramOffset 0
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        spiderSlide.damageCollision.1:
                            .dw spiderSlide.d_box.1.1

                                spiderSlide.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        spiderSlide.attackCollision.1:
                            .dw spiderSlide.a_box.1.1

                                spiderSlide.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                    spiderSlide.2:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw spiderSlide.meta.2, spiderSlide.damageCollision.2, spiderSlide.attackCollision.2

                        spiderSlide.meta.2:
                            .db 1
                            .dw spiderSlide.cell.2

                                spiderSlide.cell.2:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL7
                                    addVramOffset 4
                                    rts

                        spiderSlide.damageCollision.2:
                            .dw spiderSlide.d_box.2.1

                                spiderSlide.d_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1

                        spiderSlide.attackCollision.2:
                            .dw spiderSlide.a_box.2.1

                                spiderSlide.a_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1                            


            ;//.....................
            ;//                    .
            ;//.....................
            spiderDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            spiderDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Spider.methodTable:
            .dw Spider.method.attack, Spider.method.damage, Spider.method.death, Spider.method.spawn
            .dw Spider.method.virtualWindowCheck
            .dw Spider.method.AI, Spider.method.resetAI, Spider.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Spider.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.AI:
            jsr Spider.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Spider.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Spider.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Spider.class
            sta object.self.lo,x 
            lda #>Spider.class
            sta object.self.hi,x 
            lda #^Spider.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Spider.methodTable
            sta object.methods.lo,x
            lda #>Spider.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Spider.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Spider.method.animate:

Spider.frame = R0
Spider.frame.entryTable = R1
Spider.frame.len = D0
Spider.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Spider.AnimeTable+1,y
            sta <Spider.frame
            lda Spider.AnimeTable+2,y
            sta <Spider.frame+1

            cly
            ;Get frame type
            lda [Spider.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Spider.frame],y
            iny
            sta Spider.frame.len

            stz <Spider.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Spider.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Spider.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Spider.frame],y
            iny
            sta <Spider.frame.entryTable
            lda [Spider.frame],y
            iny
            sta <Spider.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Spider.frame.entryTable],y
            iny
            sta <D1.h
            lda <Spider.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Spider.cellTable        = A0
Spider.cell.entry       = A1
Spider.attackColPointer = A2
Spider.damageColPointer = A3

            ; Intra-frame data
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.cellTable
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.cellTable+1
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.damageColPointer
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.damageColPointer+1
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.attackColPointer
            lda [Spider.frame.entryTable],y
            iny
            sta <Spider.attackColPointer+1

Spider.cellCount = D0.l
Spider.HWsprIdx = D0.h
Spider.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Spider.HWsprIdx
            cly
            lda [Spider.cellTable],y
            iny
            sta <Spider.cellCount

.decodeCellLoop
            lda [Spider.cellTable],y
            iny
            sta <Spider.addCell
            lda [Spider.cellTable],y
            iny
            sta <Spider.addCell+1
                phy
            ldy <Spider.HWsprIdx 
            jsr.ind Spider.addCell
                ply

            dec <Spider.cellCount
          beq .buildDamageBox
            inc <Spider.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Spider.class.size = * - Spider.class