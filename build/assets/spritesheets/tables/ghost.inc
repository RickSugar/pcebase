

Ghost.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Ghost.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw ghostSlide, ghostDamage, ghostDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            ghostSlide:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 1
                    ;frame table
                    .dw ghostSlide.1
                    
                    ;Open wings
                    ghostSlide.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw ghostSlide.meta.1, ghostSlide.damageCollision.1, ghostSlide.attackCollision.1

                        ghostSlide.meta.1:
                            .db 2
                            .dw ghostSlide.cell.1, ghostSlide.cell.2

                                ghostSlide.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL8
                                    addVramOffset 0
                                    rts

                                ghostSlide.cell.2:
                                    addOffsetX 0
                                    addOffsetY 16
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL8
                                    addVramOffset 1
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        ghostSlide.damageCollision.1:
                            .dw ghostSlide.d_box.1.1

                                ghostSlide.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        ghostSlide.attackCollision.1:
                            .dw ghostSlide.a_box.1.1

                                ghostSlide.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0                         


            ;//.....................
            ;//                    .
            ;//.....................
            ghostDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            ghostDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Ghost.methodTable:
            .dw Ghost.method.attack, Ghost.method.damage, Ghost.method.death, Ghost.method.spawn
            .dw Ghost.method.virtualWindowCheck
            .dw Ghost.method.AI, Ghost.method.resetAI, Ghost.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Ghost.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.AI:
            jsr Ghost.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Ghost.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Ghost.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Ghost.class
            sta object.self.lo,x 
            lda #>Ghost.class
            sta object.self.hi,x 
            lda #^Ghost.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Ghost.methodTable
            sta object.methods.lo,x
            lda #>Ghost.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Ghost.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Ghost.method.animate:

Ghost.frame = R0
Ghost.frame.entryTable = R1
Ghost.frame.len = D0
Ghost.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Ghost.AnimeTable+1,y
            sta <Ghost.frame
            lda Ghost.AnimeTable+2,y
            sta <Ghost.frame+1

            cly
            ;Get frame type
            lda [Ghost.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Ghost.frame],y
            iny
            sta Ghost.frame.len

            stz <Ghost.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Ghost.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Ghost.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Ghost.frame],y
            iny
            sta <Ghost.frame.entryTable
            lda [Ghost.frame],y
            iny
            sta <Ghost.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Ghost.frame.entryTable],y
            iny
            sta <D1.h
            lda <Ghost.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Ghost.cellTable        = A0
Ghost.cell.entry       = A1
Ghost.attackColPointer = A2
Ghost.damageColPointer = A3

            ; Intra-frame data
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.cellTable
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.cellTable+1
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.damageColPointer
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.damageColPointer+1
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.attackColPointer
            lda [Ghost.frame.entryTable],y
            iny
            sta <Ghost.attackColPointer+1

Ghost.cellCount = D0.l
Ghost.HWsprIdx = D0.h
Ghost.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Ghost.HWsprIdx
            cly
            lda [Ghost.cellTable],y
            iny
            sta <Ghost.cellCount

.decodeCellLoop
            lda [Ghost.cellTable],y
            iny
            sta <Ghost.addCell
            lda [Ghost.cellTable],y
            iny
            sta <Ghost.addCell+1
                phy
            ldy <Ghost.HWsprIdx 
            jsr.ind Ghost.addCell
                ply

            dec <Ghost.cellCount
          beq .buildDamageBox
            inc <Ghost.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Ghost.class.size = * - Ghost.class