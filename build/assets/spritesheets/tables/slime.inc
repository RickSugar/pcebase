

Slime.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Slime.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw slimeSlide, slimeDamage, slimeDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            slimeSlide:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 3
                    ;frame table
                    .dw slimeSlide.1, slimeSlide.2, slimeSlide.3
                    
                    ;Open wings
                    slimeSlide.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw slimeSlide.meta.1, slimeSlide.damageCollision.1, slimeSlide.attackCollision.1

                        slimeSlide.meta.1:
                            .db 1
                            .dw slimeSlide.cell.1

                                slimeSlide.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL6
                                    addVramOffset 0
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        slimeSlide.damageCollision.1:
                            .dw slimeSlide.d_box.1.1

                                slimeSlide.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        slimeSlide.attackCollision.1:
                            .dw slimeSlide.a_box.1.1

                                slimeSlide.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                    slimeSlide.2:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw slimeSlide.meta.2, slimeSlide.damageCollision.2, slimeSlide.attackCollision.2

                        slimeSlide.meta.2:
                            .db 1
                            .dw slimeSlide.cell.2

                                slimeSlide.cell.2:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL6
                                    addVramOffset 2
                                    rts

                        slimeSlide.damageCollision.2:
                            .dw slimeSlide.d_box.2.1

                                slimeSlide.d_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1

                        slimeSlide.attackCollision.2:
                            .dw slimeSlide.a_box.2.1

                                slimeSlide.a_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1                            

                    slimeSlide.3:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw slimeSlide.meta.3, slimeSlide.damageCollision.3, slimeSlide.attackCollision.3

                        slimeSlide.meta.3:
                            .db 1
                            .dw slimeSlide.cell.3

                                slimeSlide.cell.3:
                                    addOffsetX 7
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL6
                                    addVramOffset 4
                                    rts

                        slimeSlide.damageCollision.3:
                            .dw slimeSlide.d_box.3.1

                                slimeSlide.d_box.3.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1

                        slimeSlide.attackCollision.3:
                            .dw slimeSlide.a_box.3.1

                                slimeSlide.a_box.3.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1 
            ;//.....................
            ;//                    .
            ;//.....................
            slimeDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            slimeDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Slime.methodTable:
            .dw Slime.method.attack, Slime.method.damage, Slime.method.death, Slime.method.spawn, Slime.method.virtualWindowCheck
            .dw Slime.method.AI, Slime.method.resetAI, Slime.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Slime.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.AI:
            jsr Slime.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Slime.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Slime.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Slime.class
            sta object.self.lo,x 
            lda #>Slime.class
            sta object.self.hi,x 
            lda #^Slime.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Slime.methodTable
            sta object.methods.lo,x
            lda #>Slime.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Slime.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Slime.method.animate:

Slime.frame = R0
Slime.frame.entryTable = R1
Slime.frame.len = D0
Slime.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Slime.AnimeTable+1,y
            sta <Slime.frame
            lda Slime.AnimeTable+2,y
            sta <Slime.frame+1

            cly
            ;Get frame type
            lda [Slime.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Slime.frame],y
            iny
            sta Slime.frame.len

            stz <Slime.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Slime.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Slime.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Slime.frame],y
            iny
            sta <Slime.frame.entryTable
            lda [Slime.frame],y
            iny
            sta <Slime.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Slime.frame.entryTable],y
            iny
            sta <D1.h
            lda <Slime.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Slime.cellTable        = A0
Slime.cell.entry       = A1
Slime.attackColPointer = A2
Slime.damageColPointer = A3

            ; Intra-frame data
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.cellTable
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.cellTable+1
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.damageColPointer
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.damageColPointer+1
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.attackColPointer
            lda [Slime.frame.entryTable],y
            iny
            sta <Slime.attackColPointer+1

Slime.cellCount = D0.l
Slime.HWsprIdx = D0.h
Slime.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Slime.HWsprIdx
            cly
            lda [Slime.cellTable],y
            iny
            sta <Slime.cellCount

.decodeCellLoop
            lda [Slime.cellTable],y
            iny
            sta <Slime.addCell
            lda [Slime.cellTable],y
            iny
            sta <Slime.addCell+1
                phy
            ldy <Slime.HWsprIdx 
            jsr.ind Slime.addCell
                ply

            dec <Slime.cellCount
          beq .buildDamageBox
            inc <Slime.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Slime.class.size = * - Slime.class