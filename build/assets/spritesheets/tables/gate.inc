

Gate.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Gate.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw gateLocked, gateOpening, gateOpen


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            gateLocked:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 1
                    ;frame table
                    .dw gateLocked.1
                    
                    gateLocked.1:
                        ; delay in frames
                        .db 20
                        ; table
                        .dw gateLocked.meta.1, gateLocked.damageCollision.1, gateLocked.attackCollision.1

                        gateLocked.meta.1:
                            .db 2
                            .dw gateLocked.cell.1, gateLocked.cell.2

                                gateLocked.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 0
                                    rts

                                gateLocked.cell.2:
                                    addOffsetX 0
                                    addOffsetY 32
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 3
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        gateLocked.damageCollision.1:
                                    .db 3,3,12,8,0

                        gateLocked.attackCollision.1:
                                    .db 3,3,12,8,0                         


            ;//.....................
            ;//                    .
            ;//.....................
            gateOpening:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 3
                    ;frame table
                    .dw gateOpening.1,gateOpening.2,gateOpening.3
                    
                    gateOpening.1:
                        ; delay in frames
                        .db 8
                        ; table
                        .dw gateOpening.meta.1, gateOpening.damageCollision.1, gateOpening.attackCollision.1

                        gateOpening.meta.1:
                            .db 2
                            .dw gateOpening.cell.1.1, gateOpening.cell.1.2

                                gateOpening.cell.1.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 0
                                    rts

                                gateOpening.cell.1.2:
                                    addOffsetX 0
                                    addOffsetY 32
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 3
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        gateOpening.damageCollision.1:
                                    .db 3,3,12,8,0

                        gateOpening.attackCollision.1:
                                    .db 3,3,12,8,0 

                    gateOpening.2:
                        ; delay in frames
                        .db 8
                        ; table
                        .dw gateOpening.meta.2, gateOpening.damageCollision.2, gateOpening.attackCollision.2

                        gateOpening.meta.2:
                            .db 2
                            .dw gateOpening.cell.2.1, gateOpening.cell.2.2

                                gateOpening.cell.2.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 4
                                    rts

                                gateOpening.cell.2.2:
                                    addOffsetX 0
                                    addOffsetY 32
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 8
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        gateOpening.damageCollision.2:
                                    .db 3,3,12,8,0

                        gateOpening.attackCollision.2:
                                    .db 3,3,12,8,0 

                    gateOpening.3:
                        ; delay in frames
                        .db 50
                        ; table
                        .dw gateOpening.meta.3, gateOpening.damageCollision.3, gateOpening.attackCollision.3

                        gateOpening.meta.3:
                            .db 2
                            .dw gateOpening.cell.3.1, gateOpening.cell.3.2

                                gateOpening.cell.3.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 10
                                    rts

                                gateOpening.cell.3.2:
                                    addOffsetX 0
                                    addOffsetY 16
                                    addAttributes SIZE32_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 12
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        gateOpening.damageCollision.3:
                                    .db 3,3,12,8,0

                        gateOpening.attackCollision.3:
                                    .db 3,3,12,8,0                                     

            ;//.....................
            ;//                    .
            ;//.....................
            gateOpen:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 1
                    ;frame table
                    .dw gateOpen.1
                    
                    ;Open wings
                    gateOpen.1:
                        ; delay in frames
                        .db 20
                        ; table
                        .dw gateOpen.meta.1, gateOpen.damageCollision.1, gateOpen.attackCollision.1

                        gateOpen.meta.1:
                            .db 2
                            .dw gateOpen.cell.1, gateOpen.cell.2

                                gateOpen.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 11
                                    rts

                                gateOpen.cell.2:
                                    addOffsetX 0
                                    addOffsetY 16
                                    addAttributes SIZE32_32 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL11
                                    addVramOffset 13
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        gateOpen.damageCollision.1:
                                    .db 3,3,12,8,0

                        gateOpen.attackCollision.1:
                                    .db 3,3,12,8,0   


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Gate.methodTable:
            .dw Gate.method.attack, Gate.method.damage, Gate.method.death, Gate.method.spawn
            .dw Gate.method.virtualWindowCheck
            .dw Gate.method.AI, Gate.method.resetAI, Gate.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Gate.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.AI:
            jsr Gate.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Gate.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Gate.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Gate.class
            sta object.self.lo,x 
            lda #>Gate.class
            sta object.self.hi,x 
            lda #^Gate.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Gate.methodTable
            sta object.methods.lo,x
            lda #>Gate.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Gate.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Gate.method.animate:

Gate.frame = R0
Gate.frame.entryTable = R1
Gate.frame.len = D0
Gate.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Gate.AnimeTable+1,y
            sta <Gate.frame
            lda Gate.AnimeTable+2,y
            sta <Gate.frame+1

            cly
            ;Get frame type
            lda [Gate.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Gate.frame],y
            iny
            sta Gate.frame.len

            stz <Gate.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Gate.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Gate.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Gate.frame],y
            iny
            sta <Gate.frame.entryTable
            lda [Gate.frame],y
            iny
            sta <Gate.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Gate.frame.entryTable],y
            iny
            sta <D1.h
            lda <Gate.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Gate.cellTable        = A0
Gate.cell.entry       = A1
Gate.attackColPointer = A2
Gate.damageColPointer = A3

            ; Intra-frame data
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.cellTable
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.cellTable+1
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.damageColPointer
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.damageColPointer+1
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.attackColPointer
            lda [Gate.frame.entryTable],y
            iny
            sta <Gate.attackColPointer+1

Gate.cellCount = D0.l
Gate.HWsprIdx = D0.h
Gate.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Gate.HWsprIdx
            cly
            lda [Gate.cellTable],y
            iny
            sta <Gate.cellCount

.decodeCellLoop
            lda [Gate.cellTable],y
            iny
            sta <Gate.addCell
            lda [Gate.cellTable],y
            iny
            sta <Gate.addCell+1
                phy
            ldy <Gate.HWsprIdx 
            jsr.ind Gate.addCell
                ply

            dec <Gate.cellCount
          beq .buildDamageBox
            inc <Gate.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Gate.class.size = * - Gate.class