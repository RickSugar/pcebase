

Rat.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Rat.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw ratRun, ratDamage, ratDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            ratRun:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 2
                    ;frame table
                    .dw ratRun.1, ratRun.2
                    
                    ;Open wings
                    ratRun.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw ratRun.meta.1, ratRun.damageCollision.1, ratRun.attackCollision.1

                        ratRun.meta.1:
                            .db 1
                            .dw ratRun.cell.1

                                ratRun.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL5
                                    addVramOffset 0
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        ratRun.damageCollision.1:
                            .dw ratRun.d_box.1.1

                                ratRun.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        ratRun.attackCollision.1:
                            .dw ratRun.a_box.1.1

                                ratRun.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0


                    ;closed wings
                    ratRun.2:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw ratRun.meta.2, ratRun.damageCollision.2, ratRun.attackCollision.2

                        ratRun.meta.2:
                            .db 1
                            .dw ratRun.cell.2

                                ratRun.cell.2:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL5
                                    addVramOffset 2
                                    rts

                        ratRun.damageCollision.2:
                            .dw ratRun.d_box.2.1, Sprite.def.EOF

                                ratRun.d_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1

                        ratRun.attackCollision.2:
                            .dw ratRun.a_box.2.1, Sprite.def.EOF

                                ratRun.a_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1                            

            ;//.....................
            ;//                    .
            ;//.....................
            ratDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            ratDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Rat.methodTable:
            .dw Rat.method.attack, Rat.method.damage, Rat.method.death, Rat.method.spawn, Rat.method.virtualWindowCheck
            .dw Rat.method.AI, Rat.method.resetAI, Rat.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Rat.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.AI:
            jsr Rat.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Rat.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Rat.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Rat.class
            sta object.self.lo,x 
            lda #>Rat.class
            sta object.self.hi,x 
            lda #^Rat.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Rat.methodTable
            sta object.methods.lo,x
            lda #>Rat.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Rat.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Rat.method.animate:

Rat.frame = R0
Rat.frame.entryTable = R1
Rat.frame.len = D0
Rat.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Rat.AnimeTable+1,y
            sta <Rat.frame
            lda Rat.AnimeTable+2,y
            sta <Rat.frame+1

            cly
            ;Get frame type
            lda [Rat.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Rat.frame],y
            iny
            sta Rat.frame.len

            stz <Rat.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Rat.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Rat.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Rat.frame],y
            iny
            sta <Rat.frame.entryTable
            lda [Rat.frame],y
            iny
            sta <Rat.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Rat.frame.entryTable],y
            iny
            sta <D1.h
            lda <Rat.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Rat.cellTable        = A0
Rat.cell.entry       = A1
Rat.attackColPointer = A2
Rat.damageColPointer = A3

            ; Intra-frame data
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.cellTable
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.cellTable+1
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.damageColPointer
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.damageColPointer+1
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.attackColPointer
            lda [Rat.frame.entryTable],y
            iny
            sta <Rat.attackColPointer+1

Rat.cellCount = D0.l
Rat.HWsprIdx = D0.h
Rat.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Rat.HWsprIdx
            cly
            lda [Rat.cellTable],y
            iny
            sta <Rat.cellCount

.decodeCellLoop
            lda [Rat.cellTable],y
            iny
            sta <Rat.addCell
            lda [Rat.cellTable],y
            iny
            sta <Rat.addCell+1
                phy
            ldy <Rat.HWsprIdx 
            jsr.ind Rat.addCell
                ply

            dec <Rat.cellCount
          beq .buildDamageBox
            inc <Rat.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Rat.class.size = * - Rat.class