

Coin.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Coin.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw coinSpin, coinCollected, coinDisappear


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            coinSpin:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 6
                    ;frame table
                    .dw coinSpin.1, coinSpin.2, coinSpin.3, coinSpin.4, coinSpin.3, coinSpin.2
                    
                    coinSpin.1:
                        ; delay in frames
                        .db 3
                        ; table
                        .dw coinSpin.meta.1, coinSpin.damageCollision.1, coinSpin.attackCollision.1

                        coinSpin.meta.1:
                            .db 1
                            .dw coinSpin.cell.1

                                coinSpin.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL10
                                    addVramOffset 0
                                    rts

                        coinSpin.damageCollision.1:
                        coinSpin.attackCollision.1:
                            .dw coinSpin.a_box.1.1

                                coinSpin.a_box.1.1:
                                    ;X1, Y1, width, Height, Type
                                    .db 3, 3, 12, 8, 0

                    coinSpin.2:
                        ; delay in frames
                        .db 3
                        ; table
                        .dw coinSpin.meta.2, coinSpin.damageCollision.2, coinSpin.attackCollision.2

                        coinSpin.meta.2:
                            .db 1
                            .dw coinSpin.cell.2

                                coinSpin.cell.2:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL10
                                    addVramOffset 1
                                    rts

                        coinSpin.damageCollision.2:
                        coinSpin.attackCollision.2:
                            .dw coinSpin.a_box.2.1

                                coinSpin.a_box.2.1:
                                    ;X1, Y1, width, Height, Type
                                    .db 3, 3, 12, 8, 0            

                   coinSpin.3:
                        ; delay in frames
                        .db 3
                        ; table
                        .dw coinSpin.meta.3, coinSpin.damageCollision.3, coinSpin.attackCollision.3

                        coinSpin.meta.3:
                            .db 1
                            .dw coinSpin.cell.3

                                coinSpin.cell.3:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL10
                                    addVramOffset 2
                                    rts

                        coinSpin.damageCollision.3:
                        coinSpin.attackCollision.3:
                            .dw coinSpin.a_box.3.1

                                coinSpin.a_box.3.1:
                                    ;X1, Y1, width, Height, Type
                                    .db 3, 3, 12, 8, 0  

                   coinSpin.4:
                        ; delay in frames
                        .db 3
                        ; table
                        .dw coinSpin.meta.4, coinSpin.damageCollision.4, coinSpin.attackCollision.4

                        coinSpin.meta.4:
                            .db 1
                            .dw coinSpin.cell.4

                                coinSpin.cell.4:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL10
                                    addVramOffset 3
                                    rts

                        coinSpin.damageCollision.4:
                        coinSpin.attackCollision.4:
                            .dw coinSpin.a_box.4.1

                                coinSpin.a_box.4.1:
                                    ;X1, Y1, width, Height, Type
                                    .db 3, 3, 12, 8, 0  

            ;//.....................
            ;//                    .
            ;//.....................
            coinCollected:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            coinDisappear:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Coin.methodTable:
            .dw Coin.method.attack, Coin.method.damage, Coin.method.death, Coin.method.spawn
            .dw Coin.method.virtualWindowCheck
            .dw Coin.method.AI, Coin.method.resetAI, Coin.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Coin.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.AI:
            jsr Coin.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Coin.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Coin.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Coin.class
            sta object.self.lo,x 
            lda #>Coin.class
            sta object.self.hi,x 
            lda #^Coin.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Coin.methodTable
            sta object.methods.lo,x
            lda #>Coin.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Coin.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Coin.method.animate:

Coin.frame = R0
Coin.frame.entryTable = R1
Coin.frame.len = D0
Coin.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Coin.AnimeTable+1,y
            sta <Coin.frame
            lda Coin.AnimeTable+2,y
            sta <Coin.frame+1

            cly
            ;Get frame type
            lda [Coin.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Coin.frame],y
            iny
            sta Coin.frame.len

            stz <Coin.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Coin.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Coin.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Coin.frame],y
            iny
            sta <Coin.frame.entryTable
            lda [Coin.frame],y
            iny
            sta <Coin.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Coin.frame.entryTable],y
            iny
            sta <D1.h
            lda <Coin.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Coin.cellTable        = A0
Coin.cell.entry       = A1
Coin.attackColPointer = A2
Coin.damageColPointer = A3

            ; Intra-frame data
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.cellTable
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.cellTable+1
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.damageColPointer
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.damageColPointer+1
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.attackColPointer
            lda [Coin.frame.entryTable],y
            iny
            sta <Coin.attackColPointer+1

Coin.cellCount = D0.l
Coin.HWsprIdx = D0.h
Coin.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Coin.HWsprIdx
            cly
            lda [Coin.cellTable],y
            iny
            sta <Coin.cellCount

.decodeCellLoop
            lda [Coin.cellTable],y
            iny
            sta <Coin.addCell
            lda [Coin.cellTable],y
            iny
            sta <Coin.addCell+1
                phy
            ldy <Coin.HWsprIdx 
            jsr.ind Coin.addCell
                ply

            dec <Coin.cellCount
          beq .buildDamageBox
            inc <Coin.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Coin.class.size = * - Coin.class