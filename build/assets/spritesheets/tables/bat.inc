

Bat.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Bat.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw batFly, batDamage, batDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            batFly:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 2
                    ;frame table
                    .dw batFly.1, batFly.2
                    
                    ;Open wings
                    batFly.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw batFly.meta.1, batFly.damageCollision.1, batFly.attackCollision.1

                        batFly.meta.1:
                            .db 1
                            .dw batFly.cell.1

                                batFly.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE32_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL4
                                    addVramOffset 0
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        batFly.damageCollision.1:
                            .dw batFly.d_box.1.1

                                batFly.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        batFly.attackCollision.1:
                            .dw batFly.a_box.1.1

                                batFly.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0


                    ;closed wings
                    batFly.2:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw batFly.meta.2, batFly.damageCollision.2, batFly.attackCollision.2

                        batFly.meta.2:
                            .db 1
                            .dw batFly.cell.2

                                batFly.cell.2:
                                    addOffsetX 2
                                    addOffsetY 1
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL4
                                    addVramOffset 2
                                    rts

                        batFly.damageCollision.2:
                            .dw batFly.d_box.2.1, Sprite.def.EOF

                                batFly.d_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1

                        batFly.attackCollision.2:
                            .dw batFly.a_box.2.1, Sprite.def.EOF

                                batFly.a_box.2.1:
                                    ;X from the left
                                    .db 2
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 1                            

            ;//.....................
            ;//                    .
            ;//.....................
            batDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            batDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Bat.methodTable:
            .dw Bat.method.attack, Bat.method.damage, Bat.method.death, Bat.method.spawn, Bat.method.virtualWindowCheck
            .dw Bat.method.AI, Bat.method.resetAI, Bat.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Bat.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.AI:
            jsr Bat.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Bat.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Bat.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Bat.class
            sta object.self.lo,x 
            lda #>Bat.class
            sta object.self.hi,x 
            lda #^Bat.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Bat.methodTable
            sta object.methods.lo,x
            lda #>Bat.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Bat.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Bat.method.animate:

Bat.frame = R0
Bat.frame.entryTable = R1
Bat.frame.len = D0
Bat.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Bat.AnimeTable+1,y
            sta <Bat.frame
            lda Bat.AnimeTable+2,y
            sta <Bat.frame+1

            cly
            ;Get frame type
            lda [Bat.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Bat.frame],y
            iny
            sta Bat.frame.len

            stz <Bat.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Bat.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Bat.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Bat.frame],y
            iny
            sta <Bat.frame.entryTable
            lda [Bat.frame],y
            iny
            sta <Bat.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Bat.frame.entryTable],y
            iny
            sta <D1.h
            lda <Bat.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Bat.cellTable        = A0
Bat.cell.entry       = A1
Bat.attackColPointer = A2
Bat.damageColPointer = A3

            ; Intra-frame data
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.cellTable
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.cellTable+1
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.damageColPointer
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.damageColPointer+1
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.attackColPointer
            lda [Bat.frame.entryTable],y
            iny
            sta <Bat.attackColPointer+1

Bat.cellCount = D0.l
Bat.HWsprIdx = D0.h
Bat.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Bat.HWsprIdx
            cly
            lda [Bat.cellTable],y
            iny
            sta <Bat.cellCount

.decodeCellLoop
            lda [Bat.cellTable],y
            iny
            sta <Bat.addCell
            lda [Bat.cellTable],y
            iny
            sta <Bat.addCell+1
                phy
            ldy <Bat.HWsprIdx 
            jsr.ind Bat.addCell
                ply

            dec <Bat.cellCount
          beq .buildDamageBox
            inc <Bat.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Bat.class.size = * - Bat.class