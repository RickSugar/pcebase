

Frog.class:

    ;..................................................
    ; Const Data                                      .
    ;..................................................

        ;...............
        ; Frame table  .
        ;...............

            Frog.AnimeTable:
                ;length: Really only needed for runtime error trapping, but make sure it matches the number of entries below
                .db 3
                ;table 
                .dw frogHop, frogDamage, frogDeath


        ;.....................
        ; Frame Definitions  .
        ;.....................

            ;//.....................
            ;//                    .
            ;//.....................
            frogHop:
                    ;type: 0 = literal data.
                    .db 0
                    ;table length
                    .db 2
                    ;frame table
                    .dw frogHop.1, frogHop.2
                    
                    frogHop.1:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw frogHop.meta.1, frogHop.damageCollision.1, frogHop.attackCollision.1

                        frogHop.meta.1:
                            .db 1
                            .dw frogHop.cell.1

                                frogHop.cell.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL9
                                    addVramOffset 0
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        frogHop.damageCollision.1:
                            .dw frogHop.d_box.1.1

                                frogHop.d_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        frogHop.attackCollision.1:
                            .dw frogHop.a_box.1.1

                                frogHop.a_box.1.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0                         

                    frogHop.2:
                        ; delay in frames
                        .db 9
                        ; table
                        .dw frogHop.meta.2, frogHop.damageCollision.2, frogHop.attackCollision.2

                        frogHop.meta.2:
                            .db 2
                            .dw frogHop.cell.2.1, frogHop.cell.2.2

                                frogHop.cell.2.1:
                                    addOffsetX 0
                                    addOffsetY 0
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL9
                                    addVramOffset 1
                                    rts

                                frogHop.cell.2.2:
                                    addOffsetX 16
                                    addOffsetY 8
                                    addAttributes SIZE16_16 | NO_V_FLIP | NO_H_FLIP | PRIOR_H | SPAL9
                                    addVramOffset 2
                                    rts

                        ; Damage and attack hitboxes are the same for this enemy.
                        frogHop.damageCollision.2:
                            .dw frogHop.d_box.2.1

                                frogHop.d_box.2.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0

                        frogHop.attackCollision.2:
                            .dw frogHop.a_box.2.1

                                frogHop.a_box.2.1:
                                    ;X from the left
                                    .db 3
                                    ;Y from the top
                                    .db 3
                                    ;width
                                    .db 12
                                    ;Height
                                    .db 8
                                    ;type
                                    .db 0  

            ;//.....................
            ;//                    .
            ;//.....................
            frogDamage:

                    ; type: 1 = current animation but blink 
                    .db 1

                    ;blink params

                    ; number of cycles to blink
                    .db 4

                    ;duration of visible frames
                    .db 30

                    ;duraction of invisible frames
                    .db 30



            ;//.....................
            ;//                    .
            ;//.....................
            frogDeath:

                    ; type: 2 = common death (blink, then cloud of smoke)
                    .db 2


    ;..................................................
    ; Public Methods                                  .
    ;..................................................

        Frog.methodTable:
            .dw Frog.method.attack, Frog.method.damage, Frog.method.death, Frog.method.spawn
            .dw Frog.method.virtualWindowCheck
            .dw Frog.method.AI, Frog.method.resetAI, Frog.method.position

        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.attack:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.damage:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.death:     

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.spawn:

        rts
        ;//.....................
        ;// In:                .
        ;//   Acc: Object #    .
        ;//   RO: VirtWinObj   .
        ;// Out:               .
        ;//   Acc: 0 -> outside.
        ;//        1 -> inside .
        ;//.....................
        Frog.method.virtualWindowCheck:        

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.AI:
            jsr Frog.method.animate

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.resetAI:

        rts
        ;//.....................
        ;//                    .
        ;//.....................
        Frog.method.position:

        rts




    ;..................................................
    ; Constructor                                     .
    ;..................................................

        ;//.....................
        ;// In: pointer RO     .
        ;// Out: Acc           .
        ;//.....................
        Frog.constructor.New:

            jsr newObject
            cpx #$ff
          bne .cont
            txa
        rts
.cont
        
            ; Setup position in level/world
            stz object.X.flt,x
            stz object.Y.flt,x
            pla
            sta object.X.lo,x
            pla
            sta object.X.hi,x
            pla
            sta object.Y.lo,x
            pla
            sta object.Y.hi,x

            ; animation, intra-frame number, down counter     
            pla
            sta object.animation,x
            pla
            sta object.frame,x

            pla
            sta object.frameCounter,x

            pla
            sta object.patternOffset.lo,x
            pla
            sta object.patternOffset.hi,x


            ; link to self (static data)
            lda #<Frog.class
            sta object.self.lo,x 
            lda #>Frog.class
            sta object.self.hi,x 
            lda #^Frog.class
            sta object.self.bank,x 

            ; link to self (methods)
            lda #<Frog.methodTable
            sta object.methods.lo,x
            lda #>Frog.methodTable
            sta object.methods.hi,x

            txa
        rts

        ;//.....................
        ;// In: Self (Acc)     .
        ;// Out:               .
        ;//.....................
        Frog.constructor.Destory:


    ;..................................................
    ; Private Methods                                 .
    ;..................................................

        ;//.....................
        ;// In: Self (X)     .
        ;// Out:               .
        ;//.....................
        Frog.method.animate:

Frog.frame = R0
Frog.frame.entryTable = R1
Frog.frame.len = D0
Frog.frameCounterReload = D0.h

            lda object.animation,x
            asl a
            tay
            lda Frog.AnimeTable+1,y
            sta <Frog.frame
            lda Frog.AnimeTable+2,y
            sta <Frog.frame+1

            cly
            ;Get frame type
            lda [Frog.frame],y
            iny
            cmp #$00
          beq .normal_process

.alternate_process
            ;for now just exit out
            rts

.normal_process
            ; Get fame table length
            lda [Frog.frame],y
            iny
            sta Frog.frame.len

            stz <Frog.frameCounterReload
            dec object.frameCounter,x
          bne .cont

.next_frame
            inc <Frog.frameCounterReload
            lda object.frame,x
            inc a
            sta object.frame,x
            cmp Frog.frame.len
          bcc .cont
            stz object.frame,x    

.cont
            sty <D1.l
            lda object.frame,x
            asl a
            adc <D1.l
            tay

            lda [Frog.frame],y
            iny
            sta <Frog.frame.entryTable
            lda [Frog.frame],y
            iny
            sta <Frog.frame.entryTable+1

            ; temporal length counter
            cly
            lda [Frog.frame.entryTable],y
            iny
            sta <D1.h
            lda <Frog.frameCounterReload
          beq .load_frame
            lda <D1.h
            sta object.frameCounter,x

.load_frame
Frog.cellTable        = A0
Frog.cell.entry       = A1
Frog.attackColPointer = A2
Frog.damageColPointer = A3

            ; Intra-frame data
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.cellTable
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.cellTable+1
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.damageColPointer
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.damageColPointer+1
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.attackColPointer
            lda [Frog.frame.entryTable],y
            iny
            sta <Frog.attackColPointer+1

Frog.cellCount = D0.l
Frog.HWsprIdx = D0.h
Frog.addCell = D1
            ;cell building
            ; get the current slot in the SATB build table
            lda hwSpriteList.buildIndex
            inc hwSpriteList.buildIndex
            sta <Frog.HWsprIdx
            cly
            lda [Frog.cellTable],y
            iny
            sta <Frog.cellCount

.decodeCellLoop
            lda [Frog.cellTable],y
            iny
            sta <Frog.addCell
            lda [Frog.cellTable],y
            iny
            sta <Frog.addCell+1
                phy
            ldy <Frog.HWsprIdx 
            jsr.ind Frog.addCell
                ply

            dec <Frog.cellCount
          beq .buildDamageBox
            inc <Frog.HWsprIdx
            inc hwSpriteList.buildIndex
          bra .decodeCellLoop

.buildDamageBox
        ; TODO

.buildAttackBox
        ; TODO


        rts


            




Frog.class.size = * - Frog.class