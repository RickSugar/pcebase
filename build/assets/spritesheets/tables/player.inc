



SpriteTable:
            .dw stand, walk, run, jump, attack, damage

    stand:
        .dw .frame, .delay

        .delay
            .db 4,5,6,7, EOF

        .frame
            .dw frame1, frame2, frame3, EOF


            .frame1

                .dw .meta, .collision

                .meta
                    .db len   ;//number of cells for meta sprite
                    .db .cell1, ..

                        .cell1

                            .xOffset
                            .db 1

                            .yOffset
                            .db 1

                            .size
                            .db 1

                            .hvFlip
                            .db 1

                            .priority
                            .db 1

                            .palette
                            .db 1

                .collision
                    .db len
                    .dw box1, box2,...

                        .box1

                            .X1
                            .db 1

                            .X2
                            .db 1

                            .Y1
                            .db 1

                            .Y2
                            .db 1

                            .type
                            .db 1






.walk

.run

.jump

.attack

.damage


