
Bat.cell
  .incspr "..\assets\spritesheets\cells\bat\Bat.png"
Bat.cell.size = * - Bat.cell

Bat.pal
  .incpal "..\assets\spritesheets\cells\bat\Bat.png", 0
Bat.pal.size = * - Bat.pal

Rat.cell
  .incspr "..\assets\spritesheets\cells\rat\Rat.png"
Rat.cell.size = * - Rat.cell

Rat.pal
  .incpal "..\assets\spritesheets\cells\rat\Rat.png", 0
Rat.pal.size = * - Rat.pal

Slime.cell
  .incspr "..\assets\spritesheets\cells\slime\Slime.png"
Slime.cell.size = * - Slime.cell

Slime.pal
  .incpal "..\assets\spritesheets\cells\slime\Slime.png", 0
Slime.pal.size = * - Slime.pal

Spider.cell
  .incspr "..\assets\spritesheets\cells\spider\Spider.png"
Spider.cell.size = * - Spider.cell

Spider.pal
  .incpal "..\assets\spritesheets\cells\spider\Spider.png", 0
Spider.pal.size = * - Spider.pal

Ghost.cell
  .incspr "..\assets\spritesheets\cells\ghost\Ghost.png"
Ghost.cell.size = * - Ghost.cell

Ghost.pal
  .incpal "..\assets\spritesheets\cells\ghost\Ghost.png", 0
Ghost.pal.size = * - Ghost.pal

Frog.cell
  .incspr "..\assets\spritesheets\cells\frog\Frog.png"
Frog.cell.size = * - Frog.cell

Frog.pal
  .incpal "..\assets\spritesheets\cells\Frog\frog.png", 0
Frog.pal.size = * - Frog.pal

Coin.cell
  .incspr "..\assets\spritesheets\cells\coin\Coin.png"
Coin.cell.size = * - Coin.cell

Coin.pal
  .incpal "..\assets\spritesheets\cells\coin\Coin.png", 0
Coin.pal.size = * - Coin.pal

Key.cell
  .incspr "..\assets\spritesheets\cells\key\Key.png"
Key.cell.size = * - Key.cell

Gate.cell
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",0,0,1,1
  .incspr "..\assets\spritesheets\cells\unused\unused.png"
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",0,16,1,1
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",0,32,1,1


  .incspr "..\assets\spritesheets\cells\gate\Gate.png",16,0,2,1
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",16,16,2,1
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",16,32,2,1

  .incspr "..\assets\spritesheets\cells\gate\Gate.png",48,0,2,1
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",48,16,2,1
  .incspr "..\assets\spritesheets\cells\gate\Gate.png",48,32,2,1

Gate.cell.size = * - Gate.cell

Gate.pal
  .incpal "..\assets\spritesheets\cells\gate\Gate.png", 0
Gate.pal.size = * - Gate.pal
