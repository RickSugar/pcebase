
;//initialize gamepad status variables
gamepad.init:
            stz b1_dly
            stz b2_dly
            stz sl_dly
            stz st_dly
            stz up_dly
            stz dn_dly
            stz lf_dly
            stz rh_dly
            stz b1_hld
            stz b2_hld
            stz sl_hld
            stz st_hld
            stz up_hld
            stz dn_hld
            stz lf_hld
            stz rh_hld
            stz soft_reset+0
            stz soft_reset+1
            stz soft_reset+2
            stz pause
            stz dpad_diag
        rts
;#end



;//
gamepad.read
            lda pause
            bne .st
            jmp .dpad

                 ;                    0            1       2     3     4      5      6         7    8      9     10      11    12      13      14   15
.dpad_tbl    .dw        ._null,._up,._rh,._up_rh,._dn,._null,._dn_rh,._null,._lf,._up_lf,._null,._null,._dn_lf,._null,._null,._null


.dpad                               ;//The directional logic is complex with states changing from UP to UP_LEFT, etc.
            lda up_sts              ;//A load table is better suited than a fall through list. It allows only
            eor dn_sts
            eor lf_sts
            eor rh_sts
            asl a
            tax
            jmp [.dpad_tbl,x]

.__button

.b1                                 ;//Button 1 is a charge type logic system.
                                    ;//Releasing the button calls the action event
            lda b1_sts
            beq .rl_set_b1
            lda b1_cntr             ;button previous held 'cause counter is positive
            cmp #$3f
            bcs .b2                 ;if the counter is maxed out, skip to next button
            inc a
            sta b1_cntr
            lda #$01
            sta b1_hld              ;make sure we keep the status as held
            bra .b2
.rl_set_b1                          ;button not currently held
            lda b1_hld
            beq .skip1              ;If button previously not held, then clear regs and continue
            jsr __action_b1         ;else action event for release of button 1
.skip1
            stz b1_hld
            stz b1_cntr

.b2
            lda b2_sts
            beq .rl_set_b2
            lda b2_hld
            bne .st
            lda b2_cntr
            inc a
            and #$03
            sta b2_cntr
            bne .st
            lda #$01
            sta b2_hld
            jsr __action_b2
            bra .st
.rl_set_b2
            stz b2_hld
            stz b2_cntr


.st
            lda st_sts
            beq .rl_set_st
            lda st_hld
            bne .exit
            lda st_cntr
            inc a
            and #$03
            sta st_cntr
            bne .exit
            lda pause
            eor #$01
            sta pause
            lda #$01
            sta st_hld
            bra .exit
.rl_set_st
            stz st_hld
            stz st_cntr

.exit
            jsr player_sprite_dma
        rts

                                    ;// no need to check if the button is set because the call was true
._up
            lda up_cntr
            inc a
            and #$00
            sta up_cntr
            bne ._up_exit
            stz dpad_diag
            jsr __action_up
._up_exit
            jmp .__button

._up_lf
            lda up_cntr
            inc a
            and #$00
            sta up_cntr
            bne ._up_lf_exit
            lda #$01
            sta dpad_diag
            jsr __action_lf
            jsr __action_up
._up_lf_exit
            jmp .__button

._up_rh
            lda up_cntr
            inc a
            and #$00
            sta up_cntr
            bne ._up_rh_exit
            lda #$01
            sta dpad_diag
            jsr __action_rh
            jsr __action_up
._up_rh_exit
            jmp .__button

._dn
            lda dn_cntr
            inc a
            and #$00
            sta dn_cntr
            bne ._dn_exit
            stz dpad_diag
            jsr __action_dn
._dn_exit
            jmp .__button

._dn_lf
            lda dn_cntr
            inc a
            and #$00
            sta dn_cntr
            bne ._dn_lf_exit
            lda #$01
            sta dpad_diag
            jsr __action_lf
            jsr __action_dn
._dn_lf_exit
            jmp .__button

._dn_rh
            lda dn_cntr
            inc a
            and #$00
            sta dn_cntr
            bne ._dn_rh_exit
            lda #$01
            sta dpad_diag
            jsr __action_rh
            jsr __action_dn
._dn_rh_exit
            jmp .__button

._lf
            lda lf_cntr
            inc a
            and #$00
            sta lf_cntr
            bne ._lf_exit
            stz dpad_diag
            jsr __action_lf
._lf_exit
            jmp .__button

._rh
            lda rh_cntr
            inc a
            and #$00
            sta rh_cntr
            bne ._rh_exit
            stz dpad_diag
            jsr __action_rh
._rh_exit
            jmp .__button

._null
            lda player_frame_cntr
            inc a
            and player_frame_cmp
            sta player_frame_cntr
            bne ._null_ll01
            lda player_frame_hld
            cmp #$02
            beq ._null_ll01
            bcc ._null_ll03
            dec a
            bra ._null_ll02
._null_ll03
            inc a
._null_ll02
            sta player_frame_hld
            asl a
            asl a
            adc player_frame_base.l
            sta [player_frame.l]
            cla
            adc player_frame_base.h
            sta [player_frame.h]

._null_ll01
            stz dpad_diag
            stz up_cntr
            stz dn_cntr
            stz lf_cntr
            stz rh_cntr
            jmp .__button

                                    ;//Note: Functions must provide the RTS
__action_b1:
            jmp [A_B1]

__action_b2:
            jmp [A_B2]

__action_up:
            jmp [A_UP]

__action_dn:
            jmp [A_DN]

__action_lf:
            jmp [A_LF]

__action_rh:
            jmp [A_RH]
;#end