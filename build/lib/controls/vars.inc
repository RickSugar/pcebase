

  .bss

        b1_dly:      .ds 1
        b2_dly:      .ds 1
        sl_dly:      .ds 1
        st_dly:      .ds 1
        up_dly:      .ds 1
        dn_dly:      .ds 1
        lf_dly:      .ds 1
        rh_dly:      .ds 1
                                ;//Indicates that the button was held on the previous read
        b1_hld:      .ds 1
        b2_hld:      .ds 1
        sl_hld:      .ds 1
        st_hld:      .ds 1
        up_hld:      .ds 1
        dn_hld:      .ds 1
        lf_hld:      .ds 1
        rh_hld:      .ds 1
        

                                ;//The number of frames the button has been held down for.
                                ;//This is used for a charge and then release system.
        b1_cntr:    .ds 1
        b2_cntr:    .ds 1
        sl_cntr:    .ds 1
        st_cntr:    .ds 1
        up_cntr:    .ds 1
        dn_cntr:    .ds 1
        lf_cntr:    .ds 1
        rh_cntr:    .ds 1
                                ;//Soft reset: is start+select. Uses a 3 variable XOR to activate the
                                ;// process, but I still need a detect 'START' button held on reset to
                                ;// avoid rapid reset.
                                ;//Pause: self explanatory
                                ;//Dpad_diag: is needed when a diagonal action should not cancel out an
                                ;// upward/downward frame position.
        soft_reset:  .ds 3
        pause:      .ds 1
        dpad_diag:  .ds 1


        A_B1:        .ds 2
        A_B2:        .ds 2
        A_ST:        .ds 2
        A_SL:        .ds 2
        A_UP:        .ds 2
        A_DN:        .ds 2
        A_LF:        .ds 2
        A_RH:        .ds 2  