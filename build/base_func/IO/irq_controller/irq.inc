
include "..\base_func\IO\irq_controller\equ.inc"

;................................................
IRQ.control     .macro

    lda #\1
    sta $1402

    .endm


;................................................
ISR.setVecMask   .macro

    lda #\1
    tsb <vector_mask

  .endm

;................................................
ISR.resetVecMask   .macro

    lda #(\1 ^ 0xff)
    trb <vector_mask

  .endm

;................................................
ISR.setVector   .macro

    php
    sei
    lda ((#\1*2) + 0.67)
    asl a
    tax
    lda \2
    sta <irq_vector_list,x
    lda \2+1
    sta <irq_vector_list+1,x
    plp

  .endm
